package group.sis.loginactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity implements View.OnClickListener{

    private Button btnRegistro,btnIngresar;
    private EditText etUsuario,etClave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnRegistro = (Button)findViewById(R.id.btnRegistro);
        btnIngresar=(Button)findViewById(R.id.btnIngresar);
        etUsuario=(EditText)findViewById(R.id.etUsuario);
        etClave=(EditText)findViewById(R.id.etclave);

        btnRegistro.setOnClickListener(this);
        btnIngresar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnIngresar)
        {
            if(etClave.getText().toString().equals("admin") && etUsuario.getText().toString().equals("admin"))
            {
                Toast.makeText(this, "Hola "+etUsuario.getText().toString(), Toast.LENGTH_LONG).show();
            }
        }else if(v.getId() ==R.id.btnRegistro){
            Intent i = new Intent(Login.this, RegistroActivity.class);
            startActivity(i);
        }

    }
}
