package group.sis.loginactivity;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import group.sis.loginactivity.bd.Almacen;
import group.sis.loginactivity.entity.Usuario;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUsuario,etNombre,etApelldos,etRut,etClave,etMatchClave;
    private Button btnRegistrar,etNacimiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        etUsuario = (EditText)findViewById(R.id.etUsuario);
        etNombre = (EditText)findViewById(R.id.etNombre);
        etApelldos = (EditText)findViewById(R.id.etApellidos);
        etRut = (EditText)findViewById(R.id.etRut);
        etNacimiento = (Button) findViewById(R.id.etNacimiento);
        etClave = (EditText)findViewById(R.id.etclave);
        etMatchClave = (EditText)findViewById(R.id.etMatchClave);

        etNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });
        btnRegistrar = (Button)findViewById(R.id.btnRegistrar);
        btnRegistrar.setOnClickListener(this);
    }

    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etNacimiento.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }

    @Override
    public void onClick(View v) {
        String mensajeError = "";
        if(etUsuario.getText().toString().length()<1)
        {
            mensajeError +=",Ingrese usuario";
        }
        if (etNombre.getText().toString().length()<1)
        {
            mensajeError +=",Ingrese nombre";
        }
        if (etApelldos.getText().toString().length()<1)
        {
            mensajeError +=",Ingrese Apellidos";
        }
        if (etRut.getText().toString().length()<10)
        {
            mensajeError +=",rut invalido";
        }
        //if (etNacimiento.getText().toString().length()<1)
        //{
           // mensajeError +=",Ingrese fecha de nacimiento";
        //}
        if (etClave.getText().toString().length()>0 && etMatchClave.getText().toString().length()>0)
        {
            String clave1=etClave.getText().toString();
            String clave2=etMatchClave.getText().toString();
            if(mensajeError.length()<1 && clave1.equals(clave2))
            {
                CrearUsuario(etUsuario,etNombre,etApelldos,etRut,etNacimiento,etClave);
            }
            else
            {
                mensajeError +=", claves no coinciden";
            }
        }
        else
        {
            mensajeError +=", Ingrese clave";
        }
        if(mensajeError.length()>=1)
        {
            Toast.makeText(this, "Error en el(los) campo(s) "+mensajeError, Toast.LENGTH_SHORT).show();
        }
    }

    private void CrearUsuario(EditText etUsuario, EditText etNombre, EditText etApelldos, EditText etRut, Button etNacimiento, EditText etClave) {
        //Toast.makeText(this, "Falta implementacion", Toast.LENGTH_SHORT).show();
        Usuario usu = new Usuario();
        usu.setUsuario(etUsuario.getText().toString());
        usu.setNombre(etNombre.getText().toString());
        usu.setApellidos(etApelldos.getText().toString());
        usu.setRut(etRut.getText().toString());
        usu.setNacimiento(etNacimiento.getText().toString());
        usu.setPss(etClave.getText().toString());
        try
        {
            Almacen.agregarUsuario(usu);
        }
        catch (NumberFormatException e)
        {
            Toast.makeText(this, "error "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }


        Toast.makeText(this, "Cantidad de registros "+Almacen.returnList().size(), Toast.LENGTH_SHORT).show();
    }

}
